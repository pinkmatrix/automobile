package com.sws.helenacastro.automobile.car.presentation.view.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sws.helenacastro.automobile.R;
import com.sws.helenacastro.automobile.car.presentation.presenter.CarListPresenterImpl;
import com.sws.helenacastro.automobile.car.presentation.presenter.CarPresenter;
import com.sws.helenacastro.automobile.car.presentation.view.CarView;
import com.sws.helenacastro.automobile.car.presentation.view.ui.activity.CarDetailActivity;
import com.sws.helenacastro.automobile.car.presentation.view.ui.adapter.CarAdapter;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.util.ToastUtils;
import com.sws.helenacastro.automobile.core.view.ui.fragment.BaseFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarListFragment extends BaseFragment implements CarView.CarList {

    public CarPresenter.CarList presenter = new CarListPresenterImpl();

    @BindView(R.id.recycler_view_car)
    RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_car_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initButterKnife();
        presenter.fetchFullCarList();
    }


    @Override
    public void initRecyclerView(List<Car> carList) {
        CarAdapter adapter = new CarAdapter(getContext(), carList);
        adapter.setOnCarClickedListener(presenter::onSelectCar);
        adapter.setOnPurchaseCartClickedListener(presenter::onClickPurchaseCart);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void goToCarDetail(Car car) {
        Intent intent = new Intent(getActivity(), CarDetailActivity.class);
//        intent.putExtra(getString(R.string.param_car), car);
        startActivity(intent);
    }

    @Override
    public void showError() {
        ToastUtils.showToast(getActivity(), "", Toast.LENGTH_SHORT);
    }

    @Override
    public void addedToPurchaseCart() {
        ToastUtils.showToast(getActivity(), "", Toast.LENGTH_SHORT);
    }

}