package com.sws.helenacastro.automobile.core.presenter;

import com.sws.helenacastro.automobile.core.view.BaseView;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class BasePresenterImpl<V extends BaseView> implements BasePresenter {

    protected V view;

    public CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected V getView() {
        return view;
    }

    @Override
    public <T> Observable<T> execute(Observable<T> observable) {
        return observable;
    }

    @Override
    public void onDestroyPresenter() {
        compositeDisposable.clear();
    }

}