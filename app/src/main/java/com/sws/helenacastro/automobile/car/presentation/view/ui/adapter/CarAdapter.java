package com.sws.helenacastro.automobile.car.presentation.view.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sws.helenacastro.automobile.R;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.util.AnimationUtils;
import com.sws.helenacastro.automobile.core.util.CurrencyUtils;

import java.util.List;
import java.util.function.Consumer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarListViewHolder> {

    private Context context;
    private List<Car> carList;
    private Consumer<Car> onCarClickedListener;
    private Consumer<Car> onPurchaseCartClickedListener;

    public CarAdapter(Context context, List<Car> carList) {
        this.context = context;
        this.carList = carList;
    }

    class CarListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardview_item_car)
        CardView cardView;

        @BindView(R.id.imgv_car)
        ImageView carImage;

        @BindView(R.id.tv_car_title)
        TextView carTitle;

        @BindView(R.id.tv_car_detail_brand)
        TextView carBrand;

        @BindView(R.id.tv_car_price)
        TextView carPrice;

        @BindView(R.id.imgv_cart)
        ImageView imgPurchaseCart;

        Car carItem;

        public CarListViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setViewHolderData(final Car item) {
            this.carItem = item;
            carTitle.setText(item.getName());
            carBrand.setText(item.getBrand());
            carPrice.setText(context.getString(R.string.total_value, CurrencyUtils.setCurrency(item.getPrice())));

            Picasso.get()
                    .load(item.getImage())
                    .placeholder(R.drawable.ic_image_placeholder)
                    .into(carImage);

            AnimationUtils.fadeIn(cardView);

            // goes to CarDetailActivity
            cardView.setOnClickListener(view -> {
                try {
//                    onCarClickedListener.accept(item);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            // Adds a car to the purchase cart;
            imgPurchaseCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
//                        onPurchaseCartClickedListener.accept(item);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        }

    }

    @NonNull
    @Override
    public CarListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_car, parent, false);
        return new CarListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarListViewHolder viewHolder, int position) {

    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public void setOnCarClickedListener(Consumer<Car> onCarClicked) {
        this.onCarClickedListener = onCarClicked;
    }

    public void setOnPurchaseCartClickedListener(Consumer<Car> onPurchaseCartClicked) {
        this.onPurchaseCartClickedListener = onPurchaseCartClicked;
    }

}