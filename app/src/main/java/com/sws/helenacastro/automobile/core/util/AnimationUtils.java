package com.sws.helenacastro.automobile.core.util;

import android.view.View;

public abstract class AnimationUtils {

    public static void fadeIn(View view){
        view.setAlpha(0f);
        view.animate()
                .alpha(1f)
                .setDuration(300)
                .start();
    }

    public static void fadeInSlow(View view) {
        if(view != null) {
            view.setAlpha(0f);
            view.animate()
                    .alpha(1f)
                    .setDuration(800)
                    .start();
        }
    }

}