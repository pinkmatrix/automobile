package com.sws.helenacastro.automobile.main.presentation.presenter;

import com.sws.helenacastro.automobile.core.presenter.BasePresenterImpl;
import com.sws.helenacastro.automobile.main.presentation.view.MainView;

public class MainPresenterImpl extends BasePresenterImpl<MainView> implements MainPresenter {

    public MainPresenterImpl() {}

    @Override
    public void initViews() {
        view.setupBottomNavigation();
    }

}