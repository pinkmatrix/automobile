package com.sws.helenacastro.automobile.core.view;

import android.view.View;

public interface BaseView {

    void showProgressBar(boolean value);

    interface Activity {
        View getView();
    }

}