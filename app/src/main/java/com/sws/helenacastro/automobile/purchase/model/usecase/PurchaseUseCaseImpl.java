package com.sws.helenacastro.automobile.purchase.model.usecase;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import java.util.List;

public class PurchaseUseCaseImpl implements PurchaseUseCase {

    public PurchaseUseCaseImpl() {}

    @Override
    public List<Car> getPurchaseList() {
        return null;
    }

    @Override
    public void removePurchaseItem(Car car) {

    }

    @Override
    public void removeAll() {

    }

}