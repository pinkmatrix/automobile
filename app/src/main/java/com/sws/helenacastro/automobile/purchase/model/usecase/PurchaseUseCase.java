package com.sws.helenacastro.automobile.purchase.model.usecase;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import java.util.List;

public interface PurchaseUseCase {

    List<Car> getPurchaseList();

    void removePurchaseItem(Car car);

    void removeAll();

}