package com.sws.helenacastro.automobile.core.util;

import java.text.NumberFormat;

public class CurrencyUtils {

    public static String setCurrency(Double n) {
        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        currencyFormatter.setMaximumFractionDigits(0);
        return currencyFormatter.format(n);
    }

}