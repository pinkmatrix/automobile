package com.sws.helenacastro.automobile.car.model.usecase;

import android.content.Context;

import com.sws.helenacastro.automobile.car.model.service.CarService;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import io.reactivex.Observable;

public class CarDetailUseCaseImpl implements CarUseCase.CarDetail {

    Context context;
    CarService service;

    public CarDetailUseCaseImpl(Context context) {
        this.context = context;
    }

    @Override
    public Observable<Car> getCarById(String id) {
        if(id != null) {
            return service.fetchCarById(id);
        } else {
            return null;
        }
    }

    @Override
    public void addToShoppingCart(Car car) {

    }

}