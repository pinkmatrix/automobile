package com.sws.helenacastro.automobile.car.model.service;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CarEndPoint {

    @GET("/carro")
    Observable<List<Car>> fetchFullCarList();

    @GET("/carro/{id}")
    Observable<Car> fetchCarById(@Path("id") String id);

}