package com.sws.helenacastro.automobile.core.model.service;

import com.sws.helenacastro.automobile.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil {

//    public static  final String BASE_URL = "http://desafiobrq.herokuapp.com/v1/carro";
    public static  final String BASE_URL = "https://5bb9343ab6ed2c0014d47524.mockapi.io/api/v1/";

    private static Retrofit retrofitInstance;

    public static Retrofit getRetrofitInstance() {
      if(retrofitInstance == null) {
        retrofitInstance = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
      }
      return retrofitInstance;
    }

}