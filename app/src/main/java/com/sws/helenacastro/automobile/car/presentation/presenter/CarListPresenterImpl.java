package com.sws.helenacastro.automobile.car.presentation.presenter;

import com.sws.helenacastro.automobile.car.model.usecase.CarListUseCaseImpl;
import com.sws.helenacastro.automobile.car.model.usecase.CarUseCase;
import com.sws.helenacastro.automobile.car.presentation.view.CarView;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.presenter.BasePresenterImpl;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CarListPresenterImpl extends BasePresenterImpl<CarView.CarList> implements CarPresenter.CarList {

    public CarUseCase.CarList useCase = new CarListUseCaseImpl();

    public CarListPresenterImpl() {}

    @Override
    public void fetchFullCarList() {
        view.showProgressBar(true);
        Disposable disposable = execute(useCase.getFullCarList())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(it -> {
                    view.showProgressBar(false);
                    view.initRecyclerView(it);
                },
                error -> {
                    view.showProgressBar(false);
                    view.showError();
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void onSelectCar(Car car) {
       view.goToCarDetail(car);
    }

    @Override
    public void onClickPurchaseCart(Car car) {
        useCase.addToShoppingCart(car);
        view.addedToPurchaseCart();
    }

}