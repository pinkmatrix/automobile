package com.sws.helenacastro.automobile.car.presentation.presenter;

import com.sws.helenacastro.automobile.car.model.usecase.CarUseCase;
import com.sws.helenacastro.automobile.car.presentation.view.CarView;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.presenter.BasePresenterImpl;

public class CarDetailPresenterImpl extends BasePresenterImpl<CarView.CarDetail> implements CarPresenter.CarDetail {

    public CarUseCase.CarDetail useCase;

    public CarDetailPresenterImpl() {
    }

    @Override
    public void addToShoppingCart(Car car, int qnt) {

    }


}