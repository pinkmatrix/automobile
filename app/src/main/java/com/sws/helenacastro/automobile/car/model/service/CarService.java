package com.sws.helenacastro.automobile.car.model.service;


import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.model.service.RetrofitUtil;
import java.util.List;

import io.reactivex.Observable;

public class CarService {

    public Observable<List<Car>> fetchFullCarList() {
        return RetrofitUtil.getRetrofitInstance().create(CarEndPoint.class).fetchFullCarList();
    }

    public Observable<Car> fetchCarById(String id) {
        return RetrofitUtil.getRetrofitInstance().create(CarEndPoint.class).fetchCarById(id);
    }

}