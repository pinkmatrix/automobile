package com.sws.helenacastro.automobile.main.presentation.presenter;

import com.sws.helenacastro.automobile.core.presenter.BasePresenter;

public interface MainPresenter extends BasePresenter {

    void initViews();

}