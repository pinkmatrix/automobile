package com.sws.helenacastro.automobile.core.view.ui.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import com.sws.helenacastro.automobile.R;
import com.sws.helenacastro.automobile.core.view.BaseView;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment implements BaseView {

    @Override
    public void showProgressBar(boolean value) {
        if (value) {
            getActivity().findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        } else {
            getActivity().findViewById(R.id.progress_bar).setVisibility(View.GONE);
        }

    }

    public void initButterKnife() {
        if (getView() != null) {
            ButterKnife.bind(this, getView());
        }
    }

}