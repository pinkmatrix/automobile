package com.sws.helenacastro.automobile.core.util;

import android.support.annotation.Nullable;

public class TextUtilsCustom {

    public static boolean isEmpty(@Nullable CharSequence str) {
        return str == null || str.length() == 0 || str.equals("");
    }

}