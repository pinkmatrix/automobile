package com.sws.helenacastro.automobile.core.model.usecase;

import com.sws.helenacastro.automobile.core.model.service.RetrofitUtil;

import io.reactivex.Observable;

public class BaseUseCaseImpl<S extends RetrofitUtil> implements BaseUseCase {

    @Override
    public <T> Observable<T> execute(Observable<T> observable) {
        return observable;
    }

}