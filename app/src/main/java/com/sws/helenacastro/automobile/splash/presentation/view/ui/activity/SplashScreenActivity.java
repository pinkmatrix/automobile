package com.sws.helenacastro.automobile.splash.presentation.view.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


import com.sws.helenacastro.automobile.R;
import com.sws.helenacastro.automobile.core.util.AnimationUtils;
import com.sws.helenacastro.automobile.main.presentation.view.ui.activity.MainActivity;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;

public class SplashScreenActivity extends AppCompatActivity {

      @Override
      protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

          Completable.complete()
                  .delay(2, TimeUnit.SECONDS)
                  .doOnComplete(this::goToMainActivity)
                  .subscribe();

          AnimationUtils.fadeInSlow(findViewById(R.id.imgv_car_detail));

      }

      private void goToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
      }

}