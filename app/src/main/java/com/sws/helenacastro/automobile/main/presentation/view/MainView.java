package com.sws.helenacastro.automobile.main.presentation.view;

import com.sws.helenacastro.automobile.core.view.BaseView;

public interface MainView extends BaseView {

    void setupBottomNavigation();

}