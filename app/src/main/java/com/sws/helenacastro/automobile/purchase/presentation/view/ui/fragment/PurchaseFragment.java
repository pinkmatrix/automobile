package com.sws.helenacastro.automobile.purchase.presentation.view.ui.fragment;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.view.ui.fragment.BaseFragment;
import com.sws.helenacastro.automobile.purchase.presentation.view.PurchaseView;

import java.util.List;

public class PurchaseFragment extends BaseFragment implements PurchaseView {


    @Override
    public void initRecyclerView(List<Car> cars) {

    }

    @Override
    public void emptyList() {

    }

    @Override
    public void updateList() {

    }

    @Override
    public void showMsgItemRemoved() {

    }

    @Override
    public void showMsgPurchaseSuccessful() {

    }

    @Override
    public void showMsgMaxLimit() {

    }

    @Override
    public void goToMainActivity() {

    }
}