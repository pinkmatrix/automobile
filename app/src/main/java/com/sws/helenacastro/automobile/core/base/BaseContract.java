package com.sws.helenacastro.automobile.core.base;

public interface BaseContract {

  interface View<P extends BaseContract.Presenter> {

    P createPresenter();
  }

  interface Presenter {

    void onAttach();

    void onDetach();
  }

}