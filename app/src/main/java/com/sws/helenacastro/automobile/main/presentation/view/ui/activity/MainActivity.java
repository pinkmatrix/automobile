package com.sws.helenacastro.automobile.main.presentation.view.ui.activity;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.sws.helenacastro.automobile.R;
import com.sws.helenacastro.automobile.car.presentation.view.ui.fragment.CarListFragment;
import com.sws.helenacastro.automobile.core.view.ui.activity.BaseActivity;
import com.sws.helenacastro.automobile.main.presentation.presenter.MainPresenter;
import com.sws.helenacastro.automobile.main.presentation.presenter.MainPresenterImpl;
import com.sws.helenacastro.automobile.main.presentation.view.MainView;
import com.sws.helenacastro.automobile.purchase.presentation.view.ui.fragment.PurchaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public MainPresenter presenter;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        presenter.initViews();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        addCarListFragment();
    }



    @Override
    public void setupBottomNavigation() {
        bottomNavigationView.setOnNavigationItemSelectedListener(onBottomNavigationViewItemSelectedListener());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onBottomNavigationViewItemSelectedListener() {
        return item -> {
            switch (item.getItemId()) {
                case R.id.navigation_car:
                    addCarListFragment();
                    break;
                case R.id.navigation_cart:
                    addPurchaseFragment();
                    break;
            }
            return false;
        };
    }

    private void setContentLayout(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_content, fragment, tag);
        fragmentTransaction.commit();
    }

    private void addCarListFragment() {
        Fragment carListFragment = new CarListFragment();
        String tag = CarListFragment.class.getSimpleName();
        setContentLayout(carListFragment, tag);
        toolbar.setTitle(R.string.car_list);
    }

    private void addPurchaseFragment() {
        Fragment purchaseFragment = new PurchaseFragment();
        String tag = PurchaseFragment.class.getSimpleName();
        setContentLayout(purchaseFragment, tag);
        toolbar.setTitle(R.string.purchase_cart);
    }

}