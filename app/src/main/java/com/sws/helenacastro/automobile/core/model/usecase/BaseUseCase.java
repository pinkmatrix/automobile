package com.sws.helenacastro.automobile.core.model.usecase;

import io.reactivex.Observable;

public interface BaseUseCase {

    <T> Observable<T> execute(Observable<T> observable);

}