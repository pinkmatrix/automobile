package com.sws.helenacastro.automobile.car.model.usecase;

import android.content.Context;

import com.sws.helenacastro.automobile.car.model.service.CarService;
import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import java.util.List;

import io.reactivex.Observable;

public class CarListUseCaseImpl implements CarUseCase.CarList {

    CarService service;

    public CarListUseCaseImpl() {}

    @Override
    public Observable<List<Car>> getFullCarList() {
        return service.fetchFullCarList();
    }

    @Override
    public void addToShoppingCart(Car car) {

    }

}