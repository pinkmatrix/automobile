package com.sws.helenacastro.automobile.core.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<P extends BaseContract.Presenter> extends RecyclerView.ViewHolder implements BaseContract.View<P> {

    protected P presenter;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void bind(int position);

    protected void populatePresenter() {
        this.presenter = createPresenter();
        onAttach();
    }

    private void onAttach() {
        if(presenter != null) {
            presenter.onAttach();
        }
    }

    private void onDetach() {
        if(presenter != null) {
            presenter.onDetach();
        }
    }

}