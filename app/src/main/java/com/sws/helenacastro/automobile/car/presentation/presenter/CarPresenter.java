package com.sws.helenacastro.automobile.car.presentation.presenter;


import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

public interface CarPresenter {

    interface CarList {
        void fetchFullCarList();
        void onSelectCar(Car car);
        void onClickPurchaseCart(Car car);
    }

    interface CarDetail {
        void addToShoppingCart(Car car, int qnt);
    }

}