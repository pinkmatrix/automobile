package com.sws.helenacastro.automobile.car.model.usecase;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;

import java.util.List;

import io.reactivex.Observable;

public interface CarUseCase {

    interface CarList {
        Observable<List<Car>> getFullCarList();
        void addToShoppingCart(Car car);
    }

    interface CarDetail {
        Observable<Car> getCarById(String id);
        void addToShoppingCart(Car car);
    }

}