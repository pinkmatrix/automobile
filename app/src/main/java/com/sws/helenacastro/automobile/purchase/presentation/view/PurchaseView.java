package com.sws.helenacastro.automobile.purchase.presentation.view;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.view.BaseView;

import java.util.List;

public interface PurchaseView extends BaseView {

    void initRecyclerView(List<Car> cars);

    void emptyList();

    void updateList();

    void showMsgItemRemoved();

    void showMsgPurchaseSuccessful();

    void showMsgMaxLimit();

    void goToMainActivity();

}