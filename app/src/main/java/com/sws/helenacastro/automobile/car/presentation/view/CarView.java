package com.sws.helenacastro.automobile.car.presentation.view;

import com.sws.helenacastro.automobile.core.model.domain.entity.Car;
import com.sws.helenacastro.automobile.core.view.BaseView;

import java.util.List;

public interface CarView extends BaseView {

    interface CarList extends CarView {
        void initRecyclerView(List<Car> carList);

        void goToCarDetail(Car car);

        void showError();

        void addedToPurchaseCart();
    }

    interface CarDetail extends CarView {
        void loadCarDetail();
    }

}