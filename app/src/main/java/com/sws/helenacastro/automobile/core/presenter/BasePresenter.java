package com.sws.helenacastro.automobile.core.presenter;

import io.reactivex.Observable;

public interface BasePresenter {

    <T> Observable<T> execute(Observable<T> observable);
    void onDestroyPresenter();

}